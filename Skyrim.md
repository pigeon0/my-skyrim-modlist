# Table of contents
1. [Textures](#Textures)
    * [Base](#Base)
    * [Clothing/Armor/Weapons](#Clothing/Armor/Weapons)
    * [Landscape](#Landscape)
    * [Characters](#Characters)
    * [Miscellaneous Texture Replacer](#Miscellaneous Texture Replacer)
    * [Animals and Creatures texture replacer](#Animals and Creatures Texture replacer)
2. [Environment](#Environment)
    * [Lighting](#Lighting)
    * [Weather](#Weather)
3. [World](#World)
    * [City Overhauls](#City Overhauls)


# Textures

## Base

### [Cleaned Skyrim SE Textures](https://www.nexusmods.com/skyrimspecialedition/mods/38775)

**NOTES**: 

* All Skyrim SE textures cleaned of compression artifacts, removed bad bicubic upscaling, and other random fixes. Improves performance too.
* Put this after USSEP:
* Downlad Kart_CSSET_Overwrite_LATEST
* Then download KART_CSSET_snow_patch
       

### [Skyrim Special Edition Upscaled Textures (SSEUT)](https://www.nexusmods.com/skyrimspecialedition/mods/34560?tab=files)

**NOTES**:

* Base Upscaled texture for everything

### [SMIM](https://www.nexusmods.com/skyrimspecialedition/mods/659)

**NOTES**:

* No requirements needed
* Base mesh mod

### [Solstheim Objects SMIM's](https://www.nexusmods.com/skyrimspecialedition/mods/53779?tab=files&file_id=221515)

**NOTES**: 

**FOMOD**: 

* Choose to add ENB Particle lights
       
       
### [Ruins Clutter Improved](https://www.nexusmods.com/skyrimspecialedition/mods/5870)

**NOTES**: 

* Included SMIM textures
* If you are using ELFX, you will have to overwrite some meshes
* Install everything in FOMOD


### [Skyrim Realistic Overhaul](https://www.moddb.com/mods/skyrim-realistic-overhaul)

**NOTES**:

* Base texture mod

### [Skyrim 2018](https://www.nexusmods.com/skyrimspecialedition/mods/20146?tab=description)

**NOTES**: 

* Base texture mod that overwrites RSO

**FOMOD**: 

* Options - Landscape, Architecture, Armor
* Renthal's chicken - yes
* Surprise(4k retexture of cloaks) - no

**Patches in FOMOD**:

* ELFX
* Luxury Suite
* Ciris Outfit
* Kaer Mohren Armor

### [Skyrim 2019](https://www.nexusmods.com/skyrimspecialedition/mods/23283?tab=files&file_id=163442)

**NOTES**: 

* Overwrite Skyrim 2018 with this


### [Skyrim 2020](https://www.nexusmods.com/skyrimspecialedition/mods/2347?tab=description)

**NOTES**: 

* Overwrite Skyrim 2019 with this
* Download 2k Textures
* Download Whiterun wall alternative textures
* [Parallex](https://www.nexusmods.com/skyrimspecialedition/mods/54860?tab=description) here


**Patches**: 

* Blended roads retuxture

## Clothing/Armor/Weapons


### [Rustic Clothing](https://www.nexusmods.com/skyrimspecialedition/mods/4703)

**NOTES**:

* Re texture of clothing 

### [aMidianBorn Book of Silence](https://www.nexusmods.com/skyrimspecialedition/mods/35382)

**NOTES**: 

* Armor/Weapon re texture that covers many weapon/armor but not all

### [Rustic Armors and Weapons](https://www.nexusmods.com/skyrimspecialedition/mods/19666)

**NOTES**: 

* Covers other stuff that aMidianBorn did not


### [Believable Weapons](https://www.nexusmods.com/skyrimspecialedition/mods/37737)

**NOTES**:

* Re-mesh of weapon to make weapons more realistic.
* Compatible with any re-textures
* Incompatible with re-meshes

**FOMOD**:

* Installtion type - full
* Refracting glass non tranparant
* Sword of Jyggalag fix

**Patches**:

* In FOMOD
           1. ALLGUD, DSR, EDS
           2. Frankly dawngard
	   3. Frankly Imperial
	   4. Refractive ebony
	   5. aMidianBornSkyforge weapon

### [LeanWolf's Better-Shaped Weapons SE](https://www.nexusmods.com/skyrimspecialedition/mods/2017)(NOT USING)

**NOTES**: 

**FOMOD**:

* Choose all
* Nighangle Blade Sheath
* Dawnbreaker Sheath ELF
* Keening Sheath
* Scimitar bling
* Keening with refraction
* Refractive glass weapons
* Stalhrim with refraction
* Dawnbreaker for ENB
* Default Dragonbone meshes(no glowy stuff)

**Patches**:

* In FOMOD;
            1. Frankfamily HD Imperial Armor and weapons(Overwrite this with LBSW)
            2. FrankFamily UltraHD Sliver Sword(Overwrite this with LBSW)
	    3. Runied Nord hero weapons(Overwrite this with LBSW)
	    4. GEMLING queen Jewelry extra feature with Dragonbone weapon not a patch
	    5. Dual Sheath Meshes


### [FrankFamily's RETEX mods](https://www.nexusmods.com/skyrimspecialedition/users/2531318?tab=user+files&BH=0)

**NOTES**: 

* To cover armor and weapon retex's previous mods did not
* Choose 2k textures for all
* Frankly HD Dragonbone and Dragonscale - Armor and Weapons
* Frankly HD Thieves Guild Armors(compatible with mesh replacers)
* Frankly HD Nightingale Armor and Weapons; 
                                           1. compatible with mods modifying the cowl's mesh
        				   2. Choose capeless and fingerless golves in FOMOD	
                                           3. Default textures
					   4. Choose the CBBE set for females
					   5. Leanwolfs Better-Shaped weapons and Dual Sheath Redux patch
					   6. Choose 2k in FOMOD
* Frankly HD Imperial Armor and Weapons:
                                        1. Is compatible with mesh replacers
                                        2. Download 2k version
				        3. Has patch for better shaped weapons
* Frankly HD Shrouded Armor:
                            1. Dont choose oblivion style in FOMOD
* Frankly HD Masqe of Clavicul Vile
* Frankly HD Dawngard Armor and Weapons:
                                        1. It's partially compatible with mesh replacers
				        2. CBBE version
					3. DSR Meshes
					    
### [Real Bows](https://www.nexusmods.com/skyrimspecialedition/mods/3144?tab=description)

**NOTES**: * Realistic bows
       * Install the alternate textures addon
    
**ALT Textures FOMOD**: * Choose all
			    
### [Quivers Redone](https://www.nexusmods.com/skyrimspecialedition/mods/65921)

**NOTES**: * Download the 2k version
       * Also the hotfix and merge




## Landscape

### [Tamrielic Textures - Landscapes](https://www.nexusmods.com/skyrimspecialedition/mods/32973)(NOT USING)

**NOTES**: 

* Base Landscape mod

**Patches**:

* in FOMOD:
            1. Majestic Mountains
            2. Better Dynamic Snow, 



### [Flully Snow](https://www.nexusmods.com/skyrimspecialedition/mods/8955)

**NOTES**: 

* Snow mod

**Patches**: 

* Blended Roads
* 3d Trees
* Majestic Mountains
* iNeed


### [Simplicity of Snow](https://www.nexusmods.com/skyrimspecialedition/mods/56235)

**NOTES**: 

* 0 worldspace edits
* 0 cell edits
* not compatible with any Riekling architecture retexture that changes the shape of the transparency
* Load ESP after anything that edits snow material shaders such as Majestic Mountains
* Patching instructions provided on the mod page
* Parallax meshes provided
* Put this after SMIM

**PATCHES**: 

* Beyond Skyrim Bruma patch
* Cities of the North
* Capital Windhelm	
* Blended Roads
* Unofficial Lux


### [Realistic Water Two](https://www.nexusmods.com/skyrimspecialedition/mods/2182)

**NOTES**: 

* Recommended for Users to patch themselves. A [tutorial](https://www.youtube.com/watch?v=YuQ0lJat_w0) is present.

**Patches**:

* Beyond Skyrim: Bruma
* Falskaar
* Open Cities 
* Wyrmstooth
* Beyond Reach 
* Saints an Seducers
    
**FOMOD**:

* ENB Rain - yes
* Water texture resolutions - none
* Smaller water drops - none
* alternate volcanic watercolor - none



### [Water for ENB](https://www.nexusmods.com/skyrimspecialedition/mods/37061?tab=description)

**NOTES**:

* GKB Waves is recommended by the author

**Compatibility**:

* Compatible with:
                  1. iNeed
                  2. Keep it Clean
		  3. 3. Realistic Needs and Diseases
		  4. 4. TDG's Wade in Water
* Works great with Depths of Skyrim 
	       
**FOMOD**: 

* Water Color - Yes
* iNeed support - no
* LOD water - default
* Water Texture Res - 2K
* Waterfalls and FX - 2k
    
**Patches**:

* Patches [here](https://www.nexusmods.com/skyrimspecialedition/mods/50394?tab=files) for 
                  1. USSEP
* Patches in FOMOD:
                   1. Landscape fixes for grass mods
	           2. Lakeview Manor Avant Garden
	           3. Half Moon Creek
        	   4. Folkvangr
	           5. Flat World Map Framework
          	   6. Expanded Towns and Cities
         	   7. Darker Volcanic water
		   8. Clear Underwater
	           9. Clear Muddy Water
		   10. Clear interior water
		   11. Atlas Map Markers
			    
**Issues**:

* Water seams, usually happens when load order is not sorted right. If not use this [fix](https://www.nexusmods.com/skyrimspecialedition/mods/9087).
* [Blacksmith forge water fix](https://www.nexusmods.com/skyrimspecialedition/mods/1291), [xEdit script](https://www.nexusmods.com/skyrimspecialedition/mods/29758)
               
**Notes** 

* Make sure nothing overwrites this mods worldspace records.




### [Majestic Mountains](https://www.nexusmods.com/skyrimspecialedition/mods/11052)

**NOTES**: * Terrain LOD redone is suggested by the mod author
       * Has DynDLod lod pack
       * Use the darkside version

**Compatibility**: 

* If using 'HD Lods' don't overwrite
* Overwriten the meshes with the meshes of water mods
* If in conflict with ELFX, use ELFX meshes
* If using ENB mesh fixes, overwrite it with Majestic Mountains

**FOMOD Options**: 

* Sun Direction - none
* Moss rocks - ESL version(needed for realistic water two)


### [Underground A dungeon texture overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/14365)

**NOTES**: 

* If third option is chosen for the imperial part; You need to select the Imperial default OR the Imperial darker exteriors together with the basic Imperial textures. Otherwise you wouldn't install the exterior parts of the Forts

### [Happy Little trees](https://www.nexusmods.com/skyrimspecialedition/mods/50961)

**FOMOD**:

* Trees - all trees
* Tree size - none
       
**Notes**: 

* Bilboard availabe at nexus page

### [Blended Roads](https://www.nexusmods.com/skyrimspecialedition/mods/8834)

**Notes**:

* Road texture mods with no alpha channel have no effect
* blending within one mesh is possible but between meshes not, this is nearly impossible in Skyrim
* he new collision can't be accurate because of avoiding hitching, character will stand some inches in the air (almost invisible)
* collision and decals like blood interact with each other, all tested decals worked fine (but it MAY be possible that some are blotchy, I haven't found some)
* Opitional file for medieval like bridges
       
**Compatibility**:

* Incompatilbe with Real Roads
* Combatible with SMIM bridges
* No issue with Fluffy Snow and Majestic Mountains
	       
**FOMOD**:

* Main Files - Really Blended Roads
* SMIM compatibility - yes


### [3D Trees and plants](https://www.nexusmods.com/skyrimspecialedition/mods/12371?tab=description)

**NOTES**:

* Only the plants are to be used in from this mod
* Download and install [this](https://drive.google.com/file/d/1KD21xuRPvISGHvgaxQlQclCo6JZFQtkG/view) and enable this esp
* Overwrite tree/meshes and textures of this mod with Happy Little Trees, if asked
* Should be loaded before RW2
* Should be loaded before City overhaul mods 
* Should be loaded after any mods that add any trees or plants in cells
* Should be loaded after any mod that affect wind animations, although recommended to remove those mods completely

### [Mari's Flora]()

**NOTES**

* Overwrite RSO with this mod

### [Voloptious Grasses](https://www.nexusmods.com/skyrimspecialedition/mods/22437)(NOT USING)

**NOTES**:

* Don't overwrite this mod with any other grass mod
* Should be loaded late in the load order
* DynDLOD stuff should be loaded after this mod
* Overwrite Maris Flora with this mod

### [The Grass Your Mother Warned About](https://www.nexusmods.com/skyrimspecialedition/mods/53064)

**NOTES**:

* This mod must be loaded under your landscape textures and above any mod that does land edits (water mods included
* load this mod after any mod that touches swordfern.dds and fallforestobject02.dds (Like Skyrim Flora Overhaul, Mari’s AIO and others)
* Landscape Fixes For Grass Mods should be loaded after this and its patch subsequently later

**Patches**:

* Landscape Fixes For Grass Mod
        
    

## Characters     

### [Racemenu](https://www.nexusmods.com/skyrimspecialedition/mods/19080)

**NOTES**: * Install anniversery edition one

### [XP32 Maximum Skeleton Special Extended](https://www.nexusmods.com/skyrimspecialedition/mods/1988)

**NOTES**: * Extended Skeleton for SSE

**FOMOD**: * Animation Rig Map - Physics extensions
       * Character Creation - Race Menu
       * Weapon Style Randomizer
       * Belt fastened quivers - none
       * daggers on back - none
       * dagger on hip - none
       * Magic - none
       * Sword on back - none
       * Sword on hip - none
       * axes on back - no
       * Sword - no

**Patces**: * in FOMOD 1. Deadly Mutilition
                   2. Joy of Perspective
		   3. Schlongs of Skyrim
		   4. Enderal

### [CBBE](https://www.nexusmods.com/skyrimspecialedition/mods/198)

**FOMOD**: * Body type - curvy
       * Underwear options - None
       * Face options - None
       * Eyebrows options - none
       * Pubic hair - check
       * SKEE - none
       * MORPH files - none

### [Tempered Skin for females](https://www.nexusmods.com/skyrimspecialedition/mods/8505)

**NOTES**: * Don't overwrite this mod to any other mod unless specifically asked

**FOMOD**: 

* Body diffuse options - BO9
       * Body normal options  - C4
       * Face diffuse options - D3
       * Face normal options  - E2
       * Scars		      - check
       * Tintmasks            - 2k
       * Beast tintmasks      - 2k

### [Schlongs of Skyrim](https://www.loverslab.com/topic/95031-schlongs-of-skyrim-se/page/2/)(NOT USING)

**NOTES**:

* Manually place the .dll and .ini in the SKSE plugin folder

**FOMOD**:

* Body type - default
* Skin texture - hairy
* Schlong addons - Average Schlong 
* Skeleton - check
* SOS Shop - no

### [Tempered Skin for males](https://www.nexusmods.com/skyrimspecialedition/mods/7902)

**NOTES**: 

* Download the nude version
* Overwrite SOS if installed

**FOMOD**: 

* Body Type    - A2, BM adjusted 
* Body diffuse - dirty hairy
* Body normal  - natural
* Face Diffuse - hard life
* Face normal  - harsh
* Scars        - yes
* tintmasks    - 2k option for both

### [High Poly Vanilla hair](https://www.nexusmods.com/skyrimspecialedition/mods/41863)

**NOTES**: 

* High poly mesh replacer
* Compatible with re-texture mods

### [Superior Lore-Friendly hair](https://www.nexusmods.com/skyrim/mods/36510)

**NOTES**: 

* 2k Texture replacer for vanilla hair

### [Masculine Khajit Textures](https://www.nexusmods.com/skyrimspecialedition/mods/186?tab=description)

**NOTES**:

* Download the SOS full version

**FOMOD**: 

* Variations - Grey Cat
* Chest textures - Furry

### [Feminine Khajit Textures](https://www.nexusmods.com/skyrimspecialedition/mods/183)

**NOTES**: * Download 2k CBEE

**FOMOD**: 

* Variations - Gray cat
* Chest variations - Furry

### [Male Dragonic Argonian Textures](https://www.nexusmods.com/skyrimspecialedition/mods/1443)

**NOTES**:

* Possible issues with Shape Atlas for Men
* Download 2k SOS full

### [Female Dragonic Argonian Textures](https://www.nexusmods.com/skyrimspecialedition/mods/1442)

**NOTES**: 

* Download 2K CBEE

### [Authentic Eyes](https://www.nexusmods.com/skyrimspecialedition/mods/36063)

**NOTES**:

* Not compatible with mods that change the vanilla eye textures
* Mods that change eye meshes may be compatible since this mod uses the vanilla meshes

### [Brows](https://www.nexusmods.com/skyrimspecialedition/mods/1062?tab=description)

**NOTES**: 

**FOMOD**:

* Full or vanilla replacer - full
* Resolution - High

### [Enhanced Blood textures](https://www.nexusmods.com/skyrimspecialedition/mods/2357?tab=files)

**NOTES**: 

* Download the standard install

**FOMOD**:

* SPID compatibility - Long distance(performance heavy script usage)
* Blood Size - Default splatter size
* Wounds  - EBT default
* Drips  - Defualt
* Screen Blood - default
* Res and Color - high res and defualt
* Alt blood textures - none

### [Bijin Warmaidens](https://www.nexusmods.com/skyrimspecialedition/mods/1825)

**NOTES**:

* plugin should be preferably at the bottom
* Load ESP after EEO if using it

**FOMOD**: 

* Characters - Lydia
* Body type  - CBBE

### [Bijin NPC's SE](https://www.nexusmods.com/skyrimspecialedition/mods/11287)

**NOTES**:

* Check the load order(Use LOOT or move their esp to the bottom manually).
* Check the load order between EEO(Ethereal Elven Overhaul) if you use it and Irileth face is having any problem.
       
**FOMOD**: 

* Options - separate 
* Characters - Carlotta, Ingun, Rikke
* Rikke face options - Wrinkles
* Body Type - CBBE

### [Bijin Wives](https://www.nexusmods.com/skyrimspecialedition/mods/11247)

**NOTES**:

* Check the load order(Use LOOT or move their esp to the bottom manually).
* Check the load order between EEO(Ethereal Elven Overhaul) if you use it and Irileth face is having any problem.

**FOMOD**: 

* Options - separate
* Characters - Camilla, Temba
* Body Type - CBBE

### [Bijin Family - Salt and Wind Textures SE](https://www.nexusmods.com/skyrimspecialedition/mods/17083)

**NOTES**:

* Vanilla friendly hair for Bijin family

**FOMOD**:

* Chech Bijin wives, NPC's and Warmaidens

### [TK Children](https://www.nexusmods.com/skyrimspecialedition/mods/5916)

**FOMOD**

* Language - English
* Features - USSEP Patch
* SS Glitch - None
* Babette eyes - Normal

**NOTES**: 

* Only needed *.tre files to be used with Simple Children
* Open the mod through explorer and delete everething except the mesh folder


### [Simple Children](https://www.nexusmods.com/skyrimspecialedition/mods/22789)

**NOTES**:

* Install update and overwrite it
* SimpleChildren.esp is an esm and esl flagged file. It should be at the top of your load order, anywhere after USSEP.
* FacegenForKids.esp is an esl flagged esp that should go fairly low in your order to overwrite any other edits to kids.
* Patches are all esl flagged esps, and can go anywhere after their respective mods except any that edit the vanilla children. Those should go below FacegenForKids.esp
* Mods that add new children are compatible
* Compatible with Wet and Cold
* If using immersive children SE, load immersive children after SimpleChildren.esp

**FOMOD**:

* Vanilla or USSEP - USSEP
* Texture choicec  - pale eye area

**PATCHES**:

* AI Overhaul
* Kidmer
* Populated Cities
* Patch for Babette glowing eyes
* AIO patch FOMOD:
                  1. Beyond Reach
	          2. Beyond Skyrim Bruma
		  3. Blackthorn
		  4. Cutting Room floor
		  5. Dragons Keep
	      	  6. EtAC
		  7. Falskaar
		  8. Gavrostead
		  9. Greater Skal village
		  10. Helgen reborn
		  11. Immersive weapon integration
		  12. Immersive world encounters
		  13. Books of Skyrim
		  14. Inconsequential NPC's
		  15. Interesting NPC's
		  16. Keld-Nar
		  17. Legacy of the Dragonborn
		  18. Moon and Star
		  19. Outlaws and Revolutionaries
	          20. Qaxe's Questorium
	          21. Rigmor of Bruma
	          22. Rigmor of Cyrodil
                  23. Settlements Expanded
	 	  24. Solitude Expansion
	          25. The forgotten City
	          26. The People of Skyrim Complete
		  27. Wyrmstooth
		  28. Adopt Avaentus Aretino options iwth USSEP and Prince and Pauper
		29. Skaal Coat addon
* Must have the USSEP updated patch [here](https://www.nexusmods.com/skyrimspecialedition/mods/39143)


## Miscellaneous Texture Replacer


### [Glorious Doors of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/32376)

**NOTES**: 

* They are all compatible with Open Cities Skyrim 

**FOMOD**: 

* Texture Size for all doors - 2k
* Rotation fix and sliding solitude door - yes

### [Scrumptious Stew HD](https://www.nexusmods.com/skyrimspecialedition/mods/21794?tab=files)

**NOTES**:

* Hearty Meat and Vegetable stew

### [Stunning Statuies of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/3375?tab=files&file_id=9840)

**FOMOD**:

* Astonich Azura
* Terrific Talos
* Ysterical Yrsgamor
* Melancholic Meridia
* Weakenend Winterhold
* Clever Clavicus
* Buculic Boethiah
* Necrotic Namira
* Magnetic Mara
* Malefic Mehrunes
* Vehement Vaermina
* Spicy Skyforge
* Fancy Falmer
* Sweet Sovngarde
* Ultra HD or HD  meshes and 2k textures for all
* Black Meridia
* Malacath Stone
* ENB winterhold meshe
* Mehrunes Dagon Black
* Skyforge non-steel
* Falmer normal copper
* Dibella and Nocturnal not chosen because of weird plasic surgerly breasts

### [Dibella statue replacer](https://www.nexusmods.com/skyrimspecialedition/mods/46747)

**NOTES**: 

* Not compatible with any mod that replaces vanilla meshes and textures of the statue of dibella

### [Better Skyrim Statues](https://www.nexusmods.com/skyrimspecialedition/mods/36601?tab=description)

**NOTES**: 

* Download Ebony Nocturnal

### [Frankly HD Masque of Clavicus Vile](https://www.nexusmods.com/skyrimspecialedition/mods/28565)

**NOTES**: 

* Download 2k version

### [The streeets of whiterun in HD](https://www.nexusmods.com/skyrimspecialedition/mods/20396)(NOT USING)

**NOTES**:

* High quality whiterun streets texture replacer

### [MD's Farmhouse](https://www.nexusmods.com/skyrimspecialedition/mods/32160)

**NOTES**:

* Download the 2k version
* Download 4k door

### [JS Dragon Claws SE](https://www.nexusmods.com/skyrimspecialedition/mods/1394)

**NOTES**:

* Download the 2k version
* Overwrite other mods with this


### [FYX - 3D Stockades - Walls and Gate](https://www.nexusmods.com/skyrimspecialedition/mods/66037)

**NOTES**: 

* Literally compatible with anything
* Download the non-parallax version

### [Uniqe Skulls HD](https://www.nexusmods.com/skyrimspecialedition/mods/52073)

**NOTES** 

* Texture replacer for unique skulls in the game
* Download 2k version

**Patches**:

* ENB light
* Skyrim Unique Treasures
* WACCF
* Uniqe Treasures

**FOMOD**: 

* Main plugin option - esp
* Skullkeys enblight patch - yes
* LODT options - none
       
### [Overlooked Dungeon Objects Retextures](https://www.nexusmods.com/skyrimspecialedition/mods/66418)

**NOTES**: 

* Self Explanatory
* Choose all options	

### [High Poly Project](https://www.nexusmods.com/skyrimspecialedition/mods/12029?tab=description)

**NOTES**: 

* Load this mod after; 
                      1. SMIM
                      2. ELFX
		      3. Rustic Clutter Collection
		      4. Particle/Subsurface Mindflux patches
	    	      5. HD Photorealistic Ivy
	    	      6. Forgotten Retex Project
	
* Load this mod before Skyrim 3d Trees and EEK's Renthal Flora Collection
* Snowfix addon in FOMOD

**Patches**: 

*  In FOMOD 
            1. Firewood snowfix Campfire
            2. No snow under roof

### [Real Hearts 4K - 2K - 1K](https://www.nexusmods.com/skyrimspecialedition/mods/66546)

**NOTES**:

* Heart Retexture

### [Forgotten Retex Project](https://www.nexusmods.com/skyrimspecialedition/mods/7849)

**NOTES**: 

* Retextures common stuff that is usually not covered by other mods

### [Uniqe Hand made signs overhaul]

**NOTES**: 

* Download 2k version

### [Elder Scroll HD - SE](https://www.nexusmods.com/skyrimspecialedition/mods/51082)

**NOTES**:

* Elder Scrolls replacer HD
* animated
* Choose the 4k version
       
**FOMOD**:

* ESP
* No mainmenu replacer
* cube mapfile from cathedral armory -no
* Elder Scroll chest - yes

**Patches**:

* LOTD
* Cunny's Improved Dwemer Glass Unofficial Material Fix
* Bards Rebonr Song

### [Elsopa's Skeleton Key](https://www.nexusmods.com/skyrimspecialedition/mods/21992)

**NOTES**: 

* Retexture for Skeeton Key

### [ElSopa HD - Small Compilation Pack SE](https://www.nexusmods.com/skyrimspecialedition/mods/29160?tab=files)

**NOTES**: 

* Compilation pack for Elsopas retextures

**FOMOD**: 

* Meadbarrel
* Grindstone
* Giant Mortar
* Burial Urns
* Briarheart red
* Bonewhawk Amulet
* Anvil
* Akatosh Amulet
* Striders and Netches
* Smelter

### [The Lovely Kettle](https://www.nexusmods.com/skyrimspecialedition/mods/36511?tab=files&file_id=142245)

**NOTES**: 

* Kettle retexture

**FOMOD**:

* Choose 2k diffuse
* Choose compressed 2k normals

### [ElSopa - Tankard HD](https://www.nexusmods.com/skyrimspecialedition/mods/43764?tab=files)

**NOTES**: 

* Download the 2k version
* Overwrite SMIM with this

### [HD-Keys Redone](https://www.nexusmods.com/skyrimspecialedition/mods/48209?tab=files&file_id=196463)

**NOTES**:

* Key retexture

### [Glorious Dummies SE](https://www.nexusmods.com/skyrimspecialedition/mods/20865?tab=files&file_id=70650)

**NOTES**:

* Dummy retexture

**FOMOD**:

* Install the 2k version

### [Hold Guard Shields](https://www.nexusmods.com/skyrimspecialedition/mods/26141)

**NOTES**: 

* High quality retexture of hold shields
* Download the 2k versoin

### [ElSopa - Papers HD SE](https://www.nexusmods.com/skyrimspecialedition/mods/33422)

**NOTES**: 

* Download the papers HD version

### [Book Covers of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/901?tab=files)

**NOTES**: 

* Download the desaturated version

### [Rustic Furniture](https://www.nexusmods.com/skyrimspecialedition/mods/17228)

**NOTES**:

* Download SE SMIM 2k
* Overwrite SMIM with this

### [CC's HQ Alduins Wall](https://www.nexusmods.com/skyrimspecialedition/mods/2638?tab=files&file_id=3816)

**NOTES**: 

* This is a simple tweak of the Texture and NormalMap of Alduins wall to make it a bit more detailed and less lumpy.


### [Renthal's workbench](https://www.nexusmods.com/skyrimspecialedition/mods/23164?tab=files&file_id=80982)

**NOTES**: 

* High poly remesh and retexture of the workbench

## Animals and Creatures Texture replacer


### [Real Rabbits SE](https://www.nexusmods.com/skyrimspecialedition/mods/29223?tab=description)

**NOTES**:

* Required for Dynamic Animal Variants SE
* Real Rabbits Diverse must be downloaded
* Mods that use the vanilla HareRace will automatically use these textures
* Mods that completely change the mesh and use a different UV mapping than vanilla will not be compatible.
* Make sure it loads after SMIM
* SMIM has a patch for Real Rabbits SE


### [Dynamic Animal Variants SE](https://www.nexusmods.com/skyrimspecialedition/mods/34763?tab=description)

**NOTES**:

* Uses BellyAches Textures
* Requires Real Rabbits
* Any retextures have a chance to appear
* Any mod that change NPC and animal records changed by this mod will have conflicts

**Patches**: 

* Daedric Beasts
* Mortal Enemies SE
* No Spinning Death animation
* Realistic Wildlife Behaviours
* Skyrim Revamped - Complete Enemy Overhaul
* Realistic Animals and Predators SE
* Violens
	 * Wild World SE

### [Immersive Dragons](https://www.nexusmods.com/skyrimspecialedition/mods/18957?tab=description)

**NOTES**: 

* Mods that make changes to dragon skeletons should be overwritten by Immersive Dragons
* Compatible with any texture mods


### [BellyAches HD Dragon Replacer](https://www.nexusmods.com/skyrimspecialedition/mods/2636)(NOT USING)

**NOTES**:

* Un-potatoes the dragons


### [HD Reworked Dragon Collection 4K](https://www.nexusmods.com/skyrimspecialedition/mods/36038?tab=files&file_id=140119)

**NOTES**: 

* Vanilla friendly 4k dragon replcer
* Covers dragonbones, scales, heartscales



### [Bears of the North](https://www.nexusmods.com/skyrimspecialedition/mods/47541)

**NOTES**: 

* Compatible with anything that doesn't touch vanilla bear records

### [Fluffworks](https://www.nexusmods.com/skyrimspecialedition/mods/56361?tab=files)

**NOTES**: 

* This mod in the current stage is incompatible with other retextures, however retextures which are vanilla upscales or follow the vanilla style (like HalkHogan's HD Reworked mods) are compatible
* Choose the quality version
* Overwrite other retextures with this mod
* Overwrite this mod with the bear mod

## [Rougeshots Skeleton replacers for creatures]

**NOTES**:

* These mods are compatible with anything that don't interfere with the kanilla sekeltons
* Install XP32MSE versions of these mods when available
* Supreme Vampire Lords
* Savage bear - replaces mesh
* Grave Gargoyles
* Nightmare Chaures
* Tyrannical Trolls
* Gardiose Giants
* Absolute Arachnophobia
* Supreme Chaurus hunters
* Wicked Werewolves
* Looming Lurkers
* Marvelous Mudcrabs
* Mighty Mammoths
* [ ] Supreme Seekers
* Bristleback Boars
* Heinous Ash Hoppers
* Honored Hounds
* Hardy Hares
* Sickening Skeevers
* Riekling Roughriders
* Sinister Spriggans
* Gritty Goats
* Astonishing Frost Atronachs
* Savage Wolves
* Dreaded Dwarven Spiders
* Notorious Netches
* Salty Slaughterfish
* Infamous Ice Wraiths
* Hulking Horkers
* Dramatic Deer
* Feral Foxes
* Bullish Bovine
* Hellish Hounds
* Callous Dwemer Centurions
* Astounding Flame Atronachs
* Supreme Dwemer Spheres
* Heartland Horses
* Marvelous Mudcrabs
* Mighty Mammoths

### [Uniqe Barbas retexture](https://www.nexusmods.com/skyrimspecialedition/mods/17540)

**NOTES**: 

* This look is based on how Barbas appeared on the ESO Summerset Cinematic


### [Frankly HD Dragon Bones](https://www.nexusmods.com/skyrimspecialedition/mods/25099)

**NOTES**: 

* Choose 4k-2k version


### [Skeleton Replacer HD - SE](https://www.nexusmods.com/skyrimspecialedition/mods/52845)

**NOTES**:

* Download 2k SSE
* The skulls for unique characters are covered in Unique Skulls HD - SE
       
**PATCHES**:

* Beast Skeleton revised(bitter edition)
* M'rissi's Tails of Troubles
* WACCF
* Skeleton eyes Enb-Light patch (you still need Particle Lights For ENB SE - Undead Creatures if you want the female draugrs,dragon priests and ghosts to have enb-light as well           , just let my enb-light patch overwrite it).﻿
* Undeath
* Majestic Mountains
* Particle Lights for ENB
* Rustic Reliefs
* Draugr Upgrades and Improvements (Draugr and Skeleton Overhaul).
* Clockwork
* Undead FX
* ELFX
* Path of Sorcery
* Extra playable skeletons SSE
* Skyrim immersive creatures
* The tools of Kagnerac
* Lux
* Deaths head Skull mask
* Sacrosant Vampires of Skyrim
* Sacrilege Minimalistic vampires of Skyrim
 
### [Beast Skeletons Revised (Bitter Edition)](https://www.nexusmods.com/skyrimspecialedition/mods/27266?tab=description)

**NOTES**: 

* Not compatible with Immersive Creatures

**Patches**:

* Draugr updates and improvements


### [Fluffworks - Better Photoreal Foxes](https://www.nexusmods.com/skyrimspecialedition/mods/65974)

**NOTES**:

* Nothing to add
* Sweet roll eyes fix

### [Gemling Queen Jewelry SE](https://www.nexusmods.com/skyrimspecialedition/mods/4294?tab=files&file_id=9441)

**NOTES**: 

* Replacers for most, if not all Jewellery in Skyrim
* Download the AIO esp
       
**FOMOD**:

* Main Modules - all
* DLC Addons - all
* Amulet textures - Gamwich textures 1024
* Ring Texture options - Gamwich Ring textures - combined 2k
* Mesh Pack Daungard - yes
       
**Patches**:

* Left hand rings

### [Rustic Amulets SE](https://www.nexusmods.com/skyrimspecialedition/mods/35485?tab=description)

**NOTES**:

* Additional Amulet Textures


### [Rustic Soulgem](https://www.nexusmods.com/skyrimspecialedition/mods/5785?tab=description)

**NOTES**: 

* Soulgem replacer that is animated and looks fucking cool
* Download the FOMOD version

**FOMOD**: 

* Textures - 2k
* Plugin type - unsorted + ESL

**Patches**: * GIST

### [Rustic Windows](https://www.nexusmods.com/skyrimspecialedition/mods/1937)

**NOTES**:

* Lore-friendly vanilla windows replacer
* Download 2k version

### [Detailed Rugs](https://www.nexusmods.com/skyrimspecialedition/mods/9030)

**NOTES**: 

* Lore friendly Rug replacer
* Download the non-clean rugs

### [Peltapalooza](https://www.nexusmods.com/skyrimspecialedition/mods/5442?tab=files)

**NOTES**:

* High quality pelt replacer
* Download the full version that includes bedroll

### [Rusting Cooking](https://www.nexusmods.com/skyrimspecialedition/mods/6142?tab=files&file_id=14122)

**NOTES**: 

* High quality cooking oven and stove replacer
* Has some food textures but will be replaced by another mod
* Overwrite this with hearty meat and vegetable stew

### [Rustic Maps](https://www.nexusmods.com/skyrimspecialedition/mods/42614?tab=files)

**NOTES**: 

* Download 4k-2k version

### [High quality food and ingredients SE](https://www.nexusmods.com/skyrimspecialedition/mods/10897?tab=files&file_id=30487)

**FOMOD**:

* Choose everything

### [Awesome potions simplified](https://www.nexusmods.com/skyrimspecialedition/mods/57607?tab=files)

**NOTES**:

* Animated potions
* No drink visual fx

**Patches**: 

* ZUPA
* ALLGUD

### [Elsopa HD - Smelters SE](https://www.nexusmods.com/skyrimspecialedition/mods/22524?tab=files)

**NOTES**:

* Smelter replacer
* Download the 2k version

### [JS Instruments of Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/51959)

**NOTES**: 

* Download the 2k version

### [JS Shrines of the Divines](https://www.nexusmods.com/skyrimspecialedition/mods/33394)

**NOTES**:

* Texture replacer for shrines
* Download the 2k version

### [ElSopa - HD Grindstone Redone SE](https://www.nexusmods.com/skyrimspecialedition/mods/58149?tab=description)

**NOTES**: 

* Grindstone replacer
* Choose the 2k version

# Environment

## Lighting


### [Relighting Skyrim](https://www.nexusmods.com/skyrimspecialedition/mods/8586)

**NOTES**: * Should be loaded after weather mods
       * Should be loadde before lighting mods
       * Compatible with 1. Enhanced Lighting for ENB Cathedral Weathers and Seasons
                         2. CLARALUX SSE - Controllable Lights Everywhere
			 3. Climates of Tamriel
			 4. Darker Nights
			 5. Dolomite Weathers - NLVA II
			 6. Dynamic Immersive Seriously Dark Dungeons - SSE
			 7. IMAGINATOR - Visual Control Device for Skyrim
			 8. Interior Floating Fog Remover
			 9. Luminosity Lighting Overhaul - The Cathedral Concept
			 10. Photorealistic Tamriel
			 11. Reduced Intensity ImageSpace Settings
			 12. Rustic Weathers And Lighting
			 13. Surreal Lighting -- vibrant and cheerful weathers and lighting
			 14. True Storms Special Edition - Thunder Rain and Weather Redone
			 15. Vivid Weathers Special Edition - a complete Weather and Visual overhaul for Skyrim SE
       * Not compatible with 1. RLO 
                             2. ELFX 
			     3. Improved Hearthfire Lighting 
			     4. Dynamic Shadows SKyrim
       * [Tutorial](https://www.youtube.com/watch?v=4ZF_iKd_5XI) for patching this with a given load order


### [Enhanced Lighting for ENB (ELE) - Special Edition](https://www.nexusmods.com/skyrimspecialedition/mods/1377)

**NOTES**:

* Patches and this mod should be loaded after Weather mods
* Same as above for light bulb mods
* Smarhed or Bashed patches should be loaded after this mod
* Not Compatible with;
                      1. RLO
                      2. ELFX
		      3. Reduced Intensity ImageSpace Settings
* Compatible with;
                  1. Relighting Skyrim - SSE
                  2. Improved Heartfire lighting Skyrim
		  3. Dynamic Shadows for Skyrim
		  4. Climates of Tamriel
		  5. Enterior Floating Fog remover
		  6. CLARALUX SSE - Controllable light everywhere
		  7. Rustic weathers and lighting
		  8. IMAGINATOR
		  9. Dolomite weathers
		  10. Surreal Lighting
		  11. Vivid Weathers
		  12. Tru Storms SE - Thunder and weather visual overhaul for Skyrim SE
		  13. Photorealistic Tamriel
		  14. Darker Nights
		  15. Dynamic Immersive Seriously Dark Dungeons - SSE
* Patches [here](https://www.nexusmods.com/skyrimspecialedition/mods/27667?tab=description) for;
                   1. JK's Skyrim
		   2. Keld-Nar
		   3. Solitude Docks
		   4. Provincial Courier Service 
		   5. Ivarstead
		   6. Shors's Stone
		   7. Kynesgrove
		   8. Falkreath
		   9. Soljund's Sinkhome
		  10. Dawnstar
		  11. Solitude Exterior addon
		  12. Rorikstead
		  13. Bring Out Your Dead
		  14. Cutting Room Floor

* And [here](https://www.nexusmods.com/skyrimspecialedition/mods/18369) for;
               1. Immersive College of Winterhold
               2. Realistic Water Two

### [ENB Helper](https://www.nexusmods.com/skyrimspecialedition/mods/23174)

**NOTES**: 

* Download the AE version

### [Particle Patch for ENB](https://www.nexusmods.com/skyrimspecialedition/mods/65720)

**NOTES**: 

* Let any other mod overwrite this mod

## Weather

### [Cathedral Weathers](https://www.nexusmods.com/skyrimspecialedition/mods/24791)

**NOTES**:

* Not compatible with other weather mods
* to patch a mod like true storms, you may wish to create a standalone esp that depends on the sound files found in true storms
* Compatible with audio overhauls with patch.
* Compatible with Obsidian Mountain Fogs. Excluded by special request.
* Safe to overwrite other weather mods

### Obsidian Weathers(NOT USING)

**NOTES**:

* Compatible with everything except weather mods
* Not compatible with mods that make nights darker unless they're made specifically for Obsidian Weathers (these are fundamentally weather mods)
* Not compatible with storm mods without a patch (these are weather mods)
* Uses only vanilla weather ID's ensuring full compatibility with mods like Frostfall, Wet and Cold, Wonders of Weather, and Beyond Skyrim
* Seasonal weathers require modification to region records and thus requires a patch for audio overhauls that add new region sounds
* Compatible with all interior lighting overhauls (ie, ELE, RS, ELFX, RLO). Minor adjustments made to torches and magelight to balance darker nights. 

**PATCHES**: 

* True Storms

### [Wonders of Weathers](https://www.nexusmods.com/skyrimspecialedition/mods/13044)

**NOTES**:

* Compatible with all mods. Load order doesn't matter.

**FOMOD**:

* No optionals should be chosen


# World

## City Overhauls

### [Capital Whiterun Expansion](https://www.nexusmods.com/skyrimspecialedition/mods/37982)

**NOTES**:

* Download the normal version
* Has exterior compatibility version too
* Expans whitrun inside and outside
* A new quest has been added
* Mods editing vanilla NPC should be OK.
* Mods editing texures should be OK
* Mods editing only interiors should be OK
* Mod's editing objects or navmesh in Town will likely need a patch
* Immersive Laundry - Compatible
* AI Overhaul - Compatible
* Bells of Skyrim - Compatible
* Astronomer's Loft - Compatible
* Download the boardwalk disabler [here](https://www.nexusmods.com/skyrimspecialedition/mods/52622?tab=files&file_id=217573)

**PATCHES**:

* Wind Districts Breezehome
* Whiterun - The city of walls
* Patches [here](https://www.nexusmods.com/skyrimspecialedition/mods/38215?tab=files) for:
                   1. Dawn of Skyrim
		   2. JK's Bannered Mare
		   3. JK's Drunken Hutsman
		   4. JK's Skyrim
		   5. JK-DOS
* Patches [here](https://www.nexusmods.com/skyrimspecialedition/mods/42766) for:
                   1. Cutting room floor
		   2. Whiterun Riverside Expansion
* Patches [here](https://www.nexusmods.com/skyrimspecialedition/mods/52622?tab=files) for: 
                   1. Fortified Whiterun
		   2. Brothermood of old
		   3. Grass patch
		   4. LoS II 
		   5. Verdant
* RS Children overhaul
* Provincial Courier service(Only for the normal version)


### [Capital Windhelm Expansion](https://www.nexusmods.com/skyrimspecialedition/mods/42990)

* Download the normal version
* Adds 40 new NPC's
* Adds stuff outside the walls
* Adds 8 new quests
* Mods editing vanilla NPCs should be OK
* Mods editing texures should be OK
* Mods editing only interiors should be ok
* Mods like JKs, DoS and others that move things around outside or place new objects will likely need a patch.
* Mods that edit Windhelm navmesh will likely need a patch.
* If you have mods that adds a lot of NPCs you might need [this](https://www.nexusmods.com/skyrimspecialedition/mods/32349)
* Incompatible with Bells of Skyrim
* Incompatible with Open Cities
* Incompatible with Snowy AF

**PATCHES**: 

* Cutting room floor
* AI Overhaul
* Embers XD
* JK's Skyrim
* High Poly NPC and KS hair JS
* Clockwork
* ICAIO
* OutlawsRefuges
* SkyrimSewers
* Windhelm gate fixes
* Danw of Skyrim
* Windhelm bridge tweaks
* [Khajit Speak](https://www.nexusmods.com/skyrimspecialedition/mods/44742?tab=files&file_id=181377)
* [Narrative Loot](https://www.nexusmods.com/skyrimspecialedition/mods/45315)
* [RS Children](https://www.nexusmods.com/skyrimspecialedition/mods/45321/?tab=posts)
* [Guard Armor Replacer](https://www.nexusmods.com/skyrimspecialedition/mods/45439?tab=files)
* [City entrances Overhaul](https://www.nexusmods.com/skyrimspecialedition/mods/47186?)


### [Windhelm Expansion - Grey Quarter](https://www.nexusmods.com/skyrimspecialedition/mods/42234)

**NOTES**

* 20+ new Interiors
* 50+ new NPC
* A new Inn
* New Shops
* A small player home
* Adds completely "new District" to Windhelm which expands the Grey Quarter
* Most changes happen in a new worldspace for better performance and easier Compatibility
* Every mod that changes Windhelm should be loaded after this mod

**PATCHES**

* Capital Windhelm Expansion
* JK's Skyrim
* Dawn of Skyrim Directors cut
* Windhelm exterior altered
* Windhelm Docks pathway
* Better Docks
* Nernies Thief pack
* Gray Quarter Overhaul


### [Enchanced Solitude SSE](https://www.nexusmods.com/skyrimspecialedition/mods/27816)

* Compilation of multiple Solitude mods
* [These](https://www.nexusmods.com/skyrimspecialedition/articles/1294) mods are alreadly included in this mod
* Mods which edit Solitude City are likely INCOMPATIBLE with Enhanced Solitude
* Mods which edit Solitude Docks should be COMPATIBLE with Enhanced Solitude
* Mods which edit Solitude Docks should be COMPATIBLE with the Enhanced Ship module
* Better Docks is COMPATIBLE
* ELFX is COMPATIBLE
* Blowing in the Wind is COMPATIBLE
* Immersive Citizens AI Overhaul is COMPATIBLE by following Load Order Rules
* Solitude Skyway is COMPATIBLE 
* Proudspire Manor mods are COMPATIBLE 
* Palaces and Castles Enhanced is COMPATIBLE
* Kainalten Keep is COMPATIBLE
* The Great City of Solitude is COMPATIBLE
* Realistic Solitude Arch is COMPATIBLE
* TPOS2 is COMPATIBLE
* Another Skyrim Solitude is NOT COMPATIBLE
* Towns and Villages Enhanced - Solitude is NOT-COMPATIBLE
* Download option for Cherry Trees
* Dowload option for ES Exterior Boost Patch
* Dowload option for ES Terrain Patch


**PATCHES**

* Serinity
* Solitude Exterior Add on
* LoTD
* Solitude Expansion
* Dev Azeza
* Blue Pallace Terrace
* Palaces and Castles enhanced
* Alternate Main wall textures
* The Flora of Skyrim Hand placed Uniques
* Bells of Skyrim
* Outlaws Refugees
* Imperial Mail
* USSEP
* Open Cities
* ES Redbag(only lowers the walls)
* Enhanced Solitude Expansion
* Enhanced Trees of Solitude
* DOS
* Immersive Laundry
* JS DoS
* JK Skyrim AIO
* LoS II TML
* RS Childrens
* Shadows of the past
* Storefront
* TFoS Cherry TRees


### [Enhanced Solitude Docks](https://www.nexusmods.com/skyrimspecialedition/mods/28349)

**NOTES**

* Compatible with Enhanced Solitude
* Combaitle with  Kainalten Keep
* Compatible with Lanterns of Skyrim 2
* Incompaitible with Haafinheim
* Incompatible with Serenity
* Incompatible with Priority of the cape
* Incompatible with Solitude Docks District
* Incompatible with The Great City of Solitude

**PATCHES**

* Enhanced Solitude
* LoS II
* Medieval Lanterns of Skyrim
* eFPS
* Open Cities
* SMIM



### [The Great Cities - Minor Cities and Towns SSE Edition](https://www.nexusmods.com/skyrimspecialedition/mods/20272?tab=description)

**NOTES**

* This mod is incompatible with any mod that significantly alters the exterior area surrounding Dawnstar, Dragon Bridge, Falkreath, Morthal, Rorikstead or Winterhold.
* This mod does NOT overaul the five major cities Markarth, Riften, Solitude, Whiterun or Windhelm.
* 
